# Assignment 2 - Agile Software Practice.

Name: Yifan Gu 20086429

## Client UI.


![][homepage1]

>>Before logging in, the homepage will show 2 buttons and a header.

![][login page]

>>Before logging in, users can not visit any pages of the website. No matter which buttons the users click on in the navigation bar, they will all be navigated to the login page.

![][register page]

>>In this page, users can register accounts. The username will be tested if it is existed. The email address will be tested if it is in a correct form.

![][homepage2]

>>After logging in, the homepage will show a welcome message and now users can are accessible to all the pages in this website.

![][post list page]

>>In this page, users can check all the posts. They can also add 'likes' for the posts they like. 

![][detail view page]

>>In this page, it will show the content of the post. If the user is the author of the post, he has the right to delete this post by clicking on the delete button.

![][post your idea page]

>>In this page, users can write posts and upload them.

![][user list page]

>>In this page, users can check the list of all the users. They can follow another user they are interested in.

## Web API CI.

Coverage report URL link: 

https://albert6429.gitlab.io/-/mymicroblog-cicd/-/jobs/379430694/artifacts/coverage/lcov-report/index.html

## Cypress Testing

Dashboard Link: https://dashboard.cypress.io/projects/2fvtud/runs/4/specs


[homepage1]: ./img/hp_notLogin.png
[login page]: ./img/LoginPage.png
[register page]: ./img/RegisterPage.png
[homepage2]: ./img/hp_Login.png
[post list page]: ./img/PostListPage.png
[detail view page]: ./img/DetailViewPage.png
[post your idea page]: ./img/PostYourIdeaPage.png
[user list page]: ./img/UserListPage.png
